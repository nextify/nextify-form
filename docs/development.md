# Guia para Desenvolvedores

**NOTA:** Raiz do modulo npm agora fica dentro de src, necessário entrar lá


Este guia irá auxiliar desenvolvedores deste projeto, smartify-form.

* Não existe teste unitário configurado ainda, está pendente
* Em nenhum momento deve ser necessário ao compilar uma nova versão o modulo vue, pois smartify-form sempre vai usar o Vue fornecido pelo usuário. (Ver mais na documentação de plugins do vue (ele carrega instancia vue do usuário))
* Todo Projeto é compilado usando Webpack e babel com ES6, seguindo os padrões de desenvolvimento do Vue

# Recomendações

* Sempre documente enquanto desenvolve, principalmente parâmetros do usuário (fica dificil adivinhar no futuro)
* Ao implementar Engine, ter em mente que atributos/métodos comuns devem ficar em abstractEngine

# Executando Demo
Leia o README.md do demo

# Problema do vue-loader

Quando criamos um projeto VueJs, o ideal é instanciar o Vue apenas uma vez e usar essa mesma instancia em todos os projetos. Isto pode parecer obvio, mas quando desenvolvemos um plugin, é comum utilizar uma nova instancia do vue na implementação do plugin, porém isso não é necessário pois quando você cria um novo plugin, você recebe da aplicação que vai ativar esse plugin uma instancia vue. ex:

```javascript

export default {
  install (Vue, options) {

    // Ativa a Estratégia de mixin para o atributo events, necessário para usar o BubbleEvents
    let strategies = Vue.config.optionMergeStrategies
    strategies.events = strategies.methods

    // Componente Principal
    Vue.component('smartify-grid', smartifyGrid)

  }
}

```

Não utilizar este procedimento pode ocasionar muitos efeitos colateráis difíceis de identificar no decorrer do desenvolvimento do projeto.

##  vue-loader

Infelizmente, existe um caso onde não é possível utilizar a instancia de Vue do projeto pai, e esse caso é quando utilizamos o vue-loader do webpack. Vue loader é um plugin do webpack, que permite importar um arquivo vue como se fosse um arquivo javascript, já convertendo as três partes do vue: template, style e script em um componente. 

O Vue-loader automaticamente instancia um novo vue, não permitindo que passemos o vue já em uso. A consequência disso, é que como plugins estão em outro modulo nodejs, vamos precisar instalar o vue tanto no modulo/projeto pai quanto no modulo do plugin. abrindo brecha para que no desenvolvimento do plugin instanciemos um vuejs novo sem gerar erro.

Então sempre prestar atenção nisso ao desenvolver novos plugins ou projetos. Se você não utilizar arquivo .vue e definir componente manualmente através de um objeto não terá este problema e não será necessário reinstanciar o vue

Outra preocupação é manter a versão do vue tanto no plugin quanto no projeto identicas para evitar conflito, além de outros modulos auxiliares do vue q não funciona com versões diferentes

# Compilando Projeto

Antigamente era preferível pré-compilar o plugin em um javascript com webpack e em seguida apenas importar no projeto do usuário, devido a facilitade de instalação pelo usuário.

Porém depois foi verificado que esta prática tem muitos efeitos colaterais:
* Alguns assets como imagens e CSS não são compilados juntos e não funciona quando usuário importa o plugin
* É Gerado código não otimizado, como bibliotecas duplicadas pelo webpack e versões diferentes gerada da mesma biblioteca.

Portanto será necessário o usuário importar o fonte e gerar um novo arquivo no projeto dele. Porém será necessário a instalação de várias dependências e verificação de versões. ou seja



**Manter sempre atualizado na documentação a lista de dependências e versões**

Os projetos que farão uso do Smartify terão dois métodos de uso, código pré compilado e compilação.
Por isso precisamos compilar um código para ficar disponível para os projetos:

Para toda alteração no SmartifyGrid será necessário recompilar. Na etapa de desenvolvimento utilize o comando:

**--------------------------- DEPRECATED DAQUI----------------------------**

Mantidos apenas a titulo de consulta, documentar na wiki e remover daqui

> npm run dev

Para um arquivo menor e já minificado executar:

> npm run prod

A compilação irá gerar um arquivo:

> /dist/build.js

Que estará no formato UMD, pronto para ser utilizado por outros projetos que já utilizam o Webpack.
Para importar basta executar o comando:

> import smartifyGrid from 'smartify-grid'

ou

> require('smartify-grid')

Para configurar o webpack para compilar para o formato UMD foi usado a seguinte configuração:

```javascript
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '/dist/',
        filename: 'build.js',
        library: 'smartifyGrid',
        libraryTarget: 'umd'
    }
```
------------------
**--------------------------- DEPRECATED ATÉ AQUI----------**

#### Iniciando Projeto Demo01 para testar enquanto desenvolve novas funcionalidades

Para desenvolver e testar o smartify, recomenda-se criar um projeto na pasta de exemplo:

Recomendável abrir 4 terminais, e executando em cada um:


**Terminal 1:**

Na pasta do demo01 (/examples/demo01)
> npm run dev

**Terminal 2:**

Na pasta do restDemo (/examples/restDemo) para iniciar o simulador de backend:
> npm run json-server

Você pode também criar um novo demo.

**Dica:** Use o demo01 como base copiando (ou usando de modelo) os seguintes arquivos:
* package.json
* webpack.config.js
* .babelrc

**Importante:** Caso as modificações não se atualizem, verifique se o smartify-grid está linkado:
> npm link smartify-grid


**IMPORTANTE:** O nodejs automaticamente procura um pacote no diretório pai, portanto não deixar modulos instalados na raiz do projeto para evitar confusão ao testar as dependencias

Exemplo:

```
/exemples
	/demo01/node_modules
/src/node_moules

[não deixar node_modules aqui na raiz]

```


## Publicando:

> npm version minor|patch

> npm publish

> git push --tags

> git checkout master

> git merge  --no-ff develop

> git push origin master


## Arquitetura

![Image of Yaktocat](./arch.jpg)

## Designer de Projeto

* Vamos permitir definir mais de um Schema, ou seja podemos passar um array de schemas, com isso podemos criar desde wizards até formulários que instanciam outros formulários
* Precisamos de suporte a Modal (Composing Components)
* Tentar Separar o projeto no máximo de componentes possível
* Basear na Arquitetura do vue-form-generator
* Criar Projeto no formato de plugin

* Precisamos estruturar nosso projeto para que suporte tanto importação via webpack,
* como através de script pré-compilado (Sem Webpack) [ Não sei como pode ser feito, pensar no futuro, por enquanto vai funcionar apenas com ES6 e webpac 2 ]

* Apesar do schema carregar do servidor, será necessário customizar algumas propriedades como aqueles que exigem funções. Por exemplo, se a pessoa selecionar um campo do tipo empresa oculta CPF e exibe CPNJ

* Conversos de Schema, vamos ter nosso próprio schema que deverá ser convertido para o vue-form-generator

SmartifyForm tem como base, três componentes principais:
### MAIN

* Componente principal do módulo, usuário poderá instanciar no template com a tag: <smartify-form>
* Faz a interface com o Servidor (vue-resource)
* Carrega e armazena Schemas ou multi-schemas (multi formulário, usado por algumas engines)
* Carrega a engine especificada pelo usuário
* Disponibiliza conjunto de métodos padrões para engines

### ENGINE

No SmartifyForm podemos utilizar vários tipos de formulários como: wizard(passo à passo), tabs (abas), modal (popup) entre outros.

Cada formulário é implementado por uma engine, podento ser extendida no futuro com novas implementações

### FIELDS CREATOR

Componente interno usado para renderizar todos os campos, baseado no vue-form-generator
Comum para todos as engines, será usado por ela para renderizar os campos na tela

Você pode encontrar documentação completa do vue-form-generator aqui:
https://icebob.gitbooks.io/vueformgenerator/content/

**Importante**: FieldsCretor apenas renderiza campo não faz nenhuma outra tarefa como salvar no servidor, submeter, esta terafa é realizada pela engine

### Template

Para facilitar a customização do formulário pelo usuário, foi criado um objeto que centraliza todo o layout do formulário.

Indepentende se o template é usado pelo componente principal, engine ou Field Creator, todas as imagens, estilos, html, fontes ou outros elementos ficarão centralizado aqui, organizado em um unico diretório e um modulo que será compilado através do Webpack


## Criando uma nova engine

* Ao emitir evento, utilize **this.emit** em vez de **this.$emit**, para que o evento seja lançado pelo componente pai para o usuário.


## Criando um novo Template

**IMPORTANTE:** Mantenha o template default mais simples possível, sem biblitoecas externas ou css, não se preocupe com usabilidade ou visual. Template Default deve ser facil customização por outros componentes.

* Quando for desenvolver um componente usando esta técnica de template, tente criar pelo menos um template bootstap focado em usabilidade e visual.


