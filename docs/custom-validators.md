# Criando Validações Customizadas

REF: https://icebob.gitbooks.io/vueformgenerator/content/fields/custom-validators.html

Validações customizadas são simplesmente funções que aceita os seguintes parâmetros:


| parameter | type |	meaning|
|-----------|------|-----------|
|value|String	|The current value of the field being validated.|
|field|schema.field object	|The schema for the field being validated.|
|model|model object |The relevant model.|
|message|string |Mensagem de Erro|

* Os atributos da validação ficam no próprio objeto schema, para acessa-los use o atributo field

ex:

```javascript
    string(value, field, model, messages = resources) {
        let res = checkEmpty(value, field.required, messages); if (res !== null) return res

        let err = []
        if (isString(value)) {
            if (!isNil(field.min) && value.length < field.min)
                err.push(msg(messages.textTooSmall, value.length, field.min))

            if (!isNil(field.max) && value.length > field.max)
                err.push(msg(messages.textTooBig, value.length, field.max))

        } else
            err.push(msg(messages.thisNotText))

        return err
    },
```

Mais exemplos no código fonte:
> src/components/fieldsCreator/utils/validators.js


