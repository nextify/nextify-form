Em schemas temos 2 níveis de configuração:


* **Nível 1:** Configuração global que serão compartilhado entre todos os projetos do Smartify (ex: Title)
* **Nível 2:** Configuração especifico deste módulo, por exemplo form, para o  formulário
* **Nível 3:** Configuração Fields, array com todos os fields (campos), como no primeiro nível, definido para todos os projetos do Smartify
* **Nível 4:** Dentro de fields podemos definir atributos especiais para o módulo atual como form

ex:

```json
{
      modelName: '',
      nextForm: '',
      prevForm: '',
      form: {

        groups:{
            users: {
                legend: 'User Detais'
            }

        }

      },
      fields: [{
           type: '',
           label: '',
           model: '',
           form: {
              readonly: true/false,
              disabled: true/false,
              row: 0,
              styleClasses: ['',''],
              errorClasses: ['','']
           },
           grid: {
              options: {}
           },
      },
          {...}
      ]
  }
}

```

**IMPORTANTE:** É possível definir um ou mais formularios, se definir mais de um formulário deve ser um objeto, ou seja indexado com o nome do formulário.
Se for definido apenas um, receberá o nome padrão: **"default"**

## Atributos do Formulário

| Atributo | Descrição            | Tipo   | Obrigatório | Padrão |
|----------|----------------------|--------|-------------|--------|
| title    | Título do Formulário | string | false       | null   |
| idField  | Nome do campo que será utilizado como identificador | string | false | 'id' |
| validateAfterLoad	| Execute validation after model loaded | boolean | false | false |
| validateAfterChanged	| Execute validation after every change | boolean | false | true
| fieldIdPrefix	| Prefix to add to every field's id. Will be prepended to ids explicity set in the schema, as well as auto-generated ones. | string	| false | null |


**Nota:** Outros atributos podem ser definidos diretamente no atributo options do componente **smartify-form**.

## Atributos dos Campos (fields)

| Atributo    | Descrição                                                                                                                                   | Tipo             | Obrigatório | Padrão |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------|------------------|-------------|--------|
| type        | Tipo do campo que será renderizado                                                                                                          | string           | true        | null   |
| model       | Nome do atributo no modelo que será vinculado a este campo.<br><br> Como é possível definir modelos de multiplos níveis, neste caso podemos vincular usando o caminho até o elemento, por exemplo:  **"endereco.rua"**. | string           | true        | null   |
| label       | Título do campo | string           | true        | null   |
| validator(*)| Validação do campo, podendo ser uma validação pré-definida (usar o nome da validação) ou função para validação customizada. Validações pré-definidas:<br>- number<br>- integer<br>- double <br>- string<br>- array<br>- date<br>- regexp<br> - email<br> - url<br>- creditCard<br>- alpha<br>- alphaNumeric   | string ou função | true        | null   |
| group       | Se definido este campo pertencerá a um grupo, que na página será renderizado dentro da tag **fieldset**<br><br>**Nota:** todos os campos desagrupados serão exibidos primeiro, seguidos dos grupos.   | string ou função | true        | null   |

**IMPORTANTE:** Existem muitos outros atributos aqui, porém eles são especificos de implementação de cada campo.

(*) **Ref:**
* https://icebob.gitbooks.io/vueformgenerator/content/validation/built-in-validators.html
* [Validações Customizadas](custom-validators.md)


