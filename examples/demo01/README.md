# Demo 01

> A Smartify Form Demo

## Build Setup

``` bash
# Instalar dependências
npm install

# Carregar servidor backend de teste
npm run json-server

# Carregar servidor frontend
npm run dev

```

Acesso o servidor backend de teste em:
> http://localhost:3000/

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
