import Vue from 'vue'
import App from './App.vue'

//import smartifyForm from 'smartify-form'
import smartifyForm from '../../../src/index'

Vue.use(smartifyForm)

new Vue({
  el: '#app',
  render: h => h(App),
})
