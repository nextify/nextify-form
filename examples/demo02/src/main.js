/**
 *
 * Demonstração de como Utilizar o Smartify-Form com ES6 e Webpack
 *
 *
 */

import Vue from 'vue'
import App from './App.vue'

//Método 01: Importa Plugin SmartifyForm do próprio diretório
import smartifyForm from '../../../src'

//Método 02: Importa como Módulo
// import smartifyForm from 'smartify-form'

// Método 03: Importando pelo binário pré-compilado
// import smartifyForm from '../../../dist/build'

// Carrega Plugin no Vue
Vue.use(smartifyForm);

new Vue({
    el: '#app',
    render: h => h(App)
});


