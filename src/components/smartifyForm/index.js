/**
 * **Created on 13/07/17**
 *
 * src/components/smartifyForm/index.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 * Componente principal (Main) que será utilizado pelo usuário
 *
 * TODO: Criar função de httprequest customizado para casos de validação ou alteração de Head ou estudar axios para configuração (Dica: ver inteceptor no axios)
 * TODO: Converter genericEvent para BubbleEvent (Ver smartify-grid) colocar implementação do buble events centralizado no smartify-tools
 */

import { each, unset, defaults, isFunction, clone, cloneDeep } from 'lodash'
import axios from 'axios'

export default {

  data () {

    // Se usuário definiu model, redireciona para engine, caso contrário, cria novo model vazio
    let formModel

    if (this.model !== undefined) {
      if (typeof this.model !== 'object') throw new TypeError('Models deve ser um objeto')
      formModel = this.model
    } else {
      formModel = {}
    }

    return {

      templateObject: null,

      /**
       * Engine Atualmente carregada para o Formulário
       * Carregado posteriormente
       */
      currentEngine: null,

      /**
       * Propriedades pros são imutáveis, ou seja não podem ser alteradas, portanto criamos um clone do atributo
       *
       *
       */
      loadedSchemas: this.schemas,

      /**
       * Model
       */
      formModel,

      /**
       * API de comunicação do Template com o Componente. Permite executar criar código customizado diretamente no template:
       * Métodos:
       *   - init (Inicialização)
       *   - event (Quando algum evento é disparado)
       */
      api: {},

    }

  },

  props: {
    /**
     * Matriz e Opções:
     * type: Tipo de formulário ex: simple, wizard
     *
     */
    options: {
      required: true,
      type: Object,
    },

    /**
     * Schema que define o formulário, deve ser objeto ou string, se string a string deverá ser uma URL e o schema será carregado do servidor
     */
    schemas: {
      required: false,
    },

    /**
     * Modelos
     */
    model: {
      required: false,
    },

    /**
     * Valor do Campo Id
     */

    id: {
      required: false,
    },

  },

  created () {

    // atalho id
    if (this.id !== undefined) this.options.id = this.id

    ///////////////////////////////////////////////////////
    // CARREGA ENGINES
    ///////////////////////////////////////////////////////
    this.loadEngines()

    ///////////////////////////////////////////////////////
    // Carrega Schemas
    ///////////////////////////////////////////////////////
    this.loadSchemas()

    if (this.api.init && isFunction(this.api.init)) {
      this.api.init(this)
    }

    ///////////////////////////////////////////////////////
    // Carrega templateObject (Objeto que centraliza todos os templates do Smartify Form)
    ///////////////////////////////////////////////////////
    this.loadTemplate()

    ///////////////////////////////////////////////////////
    // Obtém Template para este componente do template Object
    ///////////////////////////////////////////////////////
    this.$options.template = this.templateObject.smartifyForm

  },

  methods: {

    /**
     * Emite um evento vindo da Engine
     * Esta função pega todos os eventos e converte para um evento padrão do Vue de forma transaparente
     * (Na engine o evento é convertido em generic Event e aqui é convertido novamente num evento padrão)
     *
     *
     * @param event
     * @param args
     */
    onGenericEvent (event, ...args) {

      this.$emit(event, ...args)

      if (this.api.event && isFunction(this.api.event)) {
        this.api.event(this, event, ...args)
      }

    },

    /**
     * Formata Json
     * @param json
     */
    prettyJSON (json) {
      if (json) {
        json = JSON.stringify(json, undefined, 4)
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
          var cls = 'number'
          if (/^"/.test(match)) {
            if (/:$/.test(match)) {
              cls = 'key'
            } else {
              cls = 'string'
            }
          } else if (/true|false/.test(match)) {
            cls = 'boolean'
          } else if (/null/.test(match)) {
            cls = 'null'
          }
          return '<span class="' + cls + '">' + match + '</span>'
        })
      }
    },

    /**
     * Carrega Schema do servidor via API caso seja schema seja string
     */
    loadSchemas () {

      if (!this.loadedSchemas) {

        if (!this.options.schemas) {
          throw new Error('Atributo "schema" obrigatório')
        } else {
          this.loadedSchemas = this.options.schemas
        }

      }

      if (typeof this.loadedSchemas === 'string') {

        // Carrega do Servidor
        axios.get(this.loadedSchemas).then(response => {

          this.initEngine(response.data)

        })
        // TODO: Tratar Erro de conexão. Atualmente uncaught

      }

      else if (typeof this.loadedSchemas === 'object') {

        this.initEngine(this.loadedSchemas)

      }

      else {

        throw new Error('Atributo "schema", deve ser string(url da api) ou objeto')

      }

    },

    /**
     * Importa todas as engines, carrega nos componentes. Usuário define qual engine utilizar.
     * Pode ser alterado dinamicamente depois de carregado (DINÂMICO)
     *
     */
    loadEngines () {

      let engines = require.context('../formEngines/', true, /(.*)\/index\.js/)

      each(engines.keys(), key => {

        let [, engineName] = key.match(/^\.\/(.*)\/index\.js/)

        if (!this.$options.components[engineName]) {
          this.$options.components[engineName] = engines(key).default
        }

      })

    },

    /**
     * Inicializa Engine, iniciado depois do schema carregar
     */
    initEngine (schemas) {

      // Se schema for unico converte para schema default, Engines sempre vão receber em formato de multiplo schemas
      for (let [, schema] of Object.entries(schemas)) {

        if (typeof schema !== 'object' || Array.isArray(schema)) {

          schemas = {
            'default': schemas,
          }

          break

        }

      }

      // Dispara evento
      if (this.options.onSchemasLoaded) {
        schemas = this.options.onSchemasLoaded(clone(schemas))
      }

      // Formata Schema para formato do fieldsCreator (vue-form-generator)
      for (let i in schemas) {
        schemas[i] = this.formatSchemas(schemas[i])
      }

      // Carrega Engine apenas depois de carregar Schema
      this.loadedSchemas = schemas
      this.currentEngine = this.options.type || 'simple'

    },

    /**
     * Formata Schemas padrão para formato compatível com o fieldsCreator (Baseado no vue-form-generator)
     * ref: https://gitlab.smarti.io/smartify/documentation/blob/master/schema.md
     * TODO: Refactoring, criar funções, ficou complexo
     *
     * @param {{}}  schema  Atributos no formato do smartify
     * @returns {{}}  Atributos no formato do fieldsCreator (vue-form-generator)
     */
    formatSchemas (schema) {

      let form = schema.form
      let fields = schema.fields

      // limpa schema
      unset(schema, 'grid')
      unset(schema, 'form')
      unset(schema, 'fields')

      let formAttr = defaults(form, schema)

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Objeto que será retornado, no formato correto
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      let finalFormattedSchema = {

        schemaId: Math.random().toString(36).substr(2),

        // 	Execute validation after model loaded
        validateAfterLoad: formAttr.validateAfterLoad === undefined
          ? false
          : formAttr.validateAfterLoad,

        // Execute validation after every change
        validateAfterChanged: formAttr.validateAfterChanged === undefined
          ? true
          : formAttr.validateAfterChanged,

        // Prefix to add to every field's id. Will be prepended to ids explicity set in the schema, as well as auto-generated ones.
        fieldIdPrefix: formAttr.fieldIdPrefix || '',

        // Campos
        fields: [],

        // Grupos de campos
        groups: [],
      }

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Cria os grupos em formato de objeto para converter para array no final
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      let tmpGroup = {}

      // Itera sobre todos os campos
      for (let f of fields) {

        let form = f.form

        // Se null significa que não deve ser carregado no formulário
        if (form !== null) {

          // Limpa field
          unset(f, 'grid')
          unset(f, 'form')

          // Atributo field final (Vamos clonar para não modificar a fonte)
          let field = cloneDeep(
            defaults(
              form,
              f, {
                type: 'input',
              },
            ),
          )

          if (!field.model) throw new Error('Attribute model in field is required')

          ////////////////////////////////////////////////////////////////////
          // Se o campo pertence a um grupo, vamos adicionar o campo ao grupo
          ///////////////////////////////////////////////////////////////////
          if (field.group) {

            let groupName = field.group
            delete field.group

            // Se o grupo que este campo pertence não existir vamos criar
            if (!tmpGroup[groupName]) {
              tmpGroup[groupName] = {
                legend: groupName,
                fields: [],
              }
            }

            // Adiciona Campo ao grupo recém criado
            tmpGroup[groupName].fields.push(field)

          }
          ///////////////////////////////////////////////////////////////////
          // Não pertence
          ///////////////////////////////////////////////////////////////////
          else {
            finalFormattedSchema.fields.push(field)

          }

          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // Salva grupo em formato array
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          for (let [, group] of Object.entries(tmpGroup)) {
            finalFormattedSchema.groups.push(group)
          }

        }

      }

      return finalFormattedSchema

    },

    /**
     * Carrega templateObject (Objeto que centraliza todos os tempaltes do Smartify Form)
     */
    loadTemplate () {

      if (!this.options.template) this.options.template = 'default'

      if (typeof this.options.template === 'string') {
        this.templateObject = require('../../templates/' + this.options.template + '/index').default // Default por causa do ES6 export default
      } else {
        this.templateObject = this.options.template
      }

      // Carrega Api do Template
      this.api = this.templateObject.api || {}

    },

  },
}