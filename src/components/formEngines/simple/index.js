/**
 * **Created on 20/07/17**
 *
 * smartify-form/src/components/formEngines/simple/index.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 * Formulário Simples
 *
 */

import axios from 'axios'
import urljoin from 'url-join'
import { clone } from 'lodash'

import abstractEngine from '../abstractEngine'

export default {

  // Herda Atributos Base
  mixins: [abstractEngine],

  data () {

    return {

      formOptions: {
        validateAfterChanged: true,
      },

      /**
       * Se o Formulário é válido
       */
      isValid: false,

      /**
       * Lista de Erros de Validação
       */
      errors: [],

      /**
       * True se o formulário está aguardando alguma requisição do Backend
       *
       */

      loading_: false,

    }
  },

  computed: {

    /**
     * Retorna único schema
     * @returns {*}
     */
    schema () {

      let activeSchema = this.options.activeSchema || 'default'

      if (this.schemas[activeSchema] === undefined) {
        throw new Error(`Schema ${activeSchema} não encontrado`)
      }

      return this.schemas[activeSchema]

    },

    /**
     * Retorna nome do campo Identificador do formulário
     *
     * @returns {*|string}
     */
    nameOfIdField () {
      return this.schema['idField'] || 'id'
    },

    /**
     * Texto do Botão de Salvar
     *
     * @returns {*}
     */
    buttonLabel () {

      if (this.isNew) {
        return this.options.saveButtonLabel || 'Salvar'
      } else {
        return this.options.updateButtonLabel || 'Atualizar'
      }

    },

    /**
     * Mensagem exibida enquanto formulário aguarda requisição para o Backend
     */
    waitMessage(){

      return this.options.waitMessage || 'Aguarde...'

    },

    /**
     * Define se Formulário está em modo Loading ou não
     * Realiza tarefas relacionada
     *
     */
    loading: {

      set (isLoading) {

        if (isLoading) {

          this.$nextTick(function () {
            this.loading_ = true
            this.emit('loading', this)
          })

        } else {

          this.$nextTick(function () {
            this.loading_ = false
            this.emit('loaded', this)
          })

        }

      },

      get () {
        return this.loading_
      },
    },

  },

  methods: {

    /**
     * Inicialização, chamado pelo created na base
     */
    init () {

      // Se id estiver definido no modelo, carrega formulário
      if (this.nameOfIdField && this.model[this.nameOfIdField] !== undefined && this.options.id === undefined) {
        this.options.id = this.model[this.nameOfIdField]
      }

      // Carrega modelo se for no modo Update ou Valores Default
      if (this.options.id === undefined) {

        this.loadDefault()

      } else if (this.options.apiUrl) {

        // Se url não estiver definida não precisa carregar

        this.loadModel(this.options.id, this.options.apiUrl)

      }

    },

    /**
     * Define Valor para Id
     */
    setId (newValue) {

      this.$set(this.options, 'id', newValue)
      this.$set(this.model, 'id', this.id)

    },

    /**
     * Carrega Valores Padrões no Modelo
     */
    loadDefault () {

      // Carrega valores padrão
      for (let field of this.schema.fields) {

        let fieldName = field.model
        let defaultValue = field.default

        // Se existir um valor padrão e valor não estiver já definido no modelo, define valor padrão
        if (defaultValue !== undefined && this.model[fieldName] !== undefined) {
          this.$set(this.model, fieldName, defaultValue)
        }

      }

      if (this.nameOfIdField) {
        this.$set(this.model, this.nameOfIdField, undefined)
      }

    },

    /**
     * Emite um evento genérico. Utilizar this.emit() em vez de this.$emit()
     *
     * @param isValid
     * @param errors
     */
    onValidated (isValid, errors) {

      this.isValid = isValid
      this.errors = errors

      this.emit('validated', this, isValid, errors)
    },

    /**
     * Salva Modelo, Verificando validação antes
     */
    save () {

      let isValid = this.$refs.fieldsObj.validate()

      if (isValid) {
        this.saveModel()
      }

    },

    /**
     * Carrega Model do Backend aparti do ID
     * @param id      Id que será recuperado do Backend
     * @param apiUrl  URL do Backend
     *
     * @returns {Promise.<TResult>}
     */
    loadModel (id, apiUrl) {

      this.loading = true

      return axios.get(urljoin(apiUrl, id)).then(response => {

        this.updateModel(response.data)
        this.loading = false

        // Força id
        // noinspection EqualityComparisonWithCoercionJS
        if (this.model[this.nameOfIdField] != id) throw new Error('Id de verificação retornado pelo backend difere do id do formulário')

      }).catch(err => {

        this.loading = false
        throw err

      })

    },

    /**
     * Salva Modelo
     */
    saveModel () {

      this.emit('save', this, clone(this.model), clone(this.isNew))

      if (this.options.apiUrl) {

        // Força valor de id no modelo
        this.$set(this.model, 'id', this.id)

        if (this.isNew) {
          return this.postModel(this.options.apiUrl)
        } else {
          return this.putModel(this.id, this.options.apiUrl)
        }

      }

    },

    /**
     * Realiza um Requisição POST para o Servidor
     *
     * @param apiUrl
     * @returns {Promise.<TResult>}
     */
    postModel (apiUrl) {

      // Carrega do Servidor
      this.loading = true

      return axios
      .post(apiUrl, this.model).then(response => {

        this.updateModel(response.data)
        this.loading = false

        // Grava Novo Id
        this.$set(this.options, 'id', this.model[this.nameOfIdField])

        this.emit('saveSuccess', this, clone(this.id), clone(this.model), true)

      })
      .catch(err => {

        this.loading = false
        throw err

      })

    },

    /**
     * Realiza um Update para o Servidor
     *
     * @param id
     * @param apiUrl
     * @returns {Promise.<TResult>}
     */
    putModel (id, apiUrl) {

      // Carrega do Servidor
      this.loading = true
      return axios.put(urljoin(apiUrl, id), this.model).then(response => {

        this.updateModel(response.data)
        this.loading = false

        this.emit('saveSuccess', this, clone(this.id), clone(this.model), false)

      }).catch(err => {

        this.loading = false
        throw err

      })

    },

  },
}