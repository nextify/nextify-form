/**
 * **Created on 20/07/17**
 *
 * smartify-form/src/components/formEngines/abstractEngine.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 * Objeto Abstrado para implementação de Engines
 *
 */



export default {

  template: '<strong> Erro ao carregar Template </strong>',

  props: {
    /**
     * Objeto de template com todos os templates
     */
    templateObject: {
      type: Object,
      required: true,
    },

    /**
     * Nome desta engine
     */
    name: {
      type: String,
      required: true,
    },

    /**
     * Schemas
     */
    schemas: {
      required: true,
    },

    /**
     * Opções
     */
    options: {
      required: true,
      type: Object,
    },

    /**
     * Model
     */
    model: {
      required: true,
      type: Object,
    },

  },

  computed: {

    /**
     * Se este form é um novo ou atualização
     * @returns {boolean}
     */
    isNew () {
      return this.options.id === undefined
    },

    /**
     * id Atual, podemos definir novo id
     *
     */
    id: {

      get () {
        return this.options.id
      },

      set (newValue) {

        this.setId(newValue)

      },

    },
  },

  created () {

    this.init()

    // Carrega template correto automaticamente
    this.$options.template = this.templateObject.engine[this.name]

  },

  methods: {

    /**
     * Abstrato: Para ser usado pela engine como código de inicialização
     */
    init () {

    },

    /**
     * Define Valor para Id
     */
    setId (newValue) {

      this.$set(this.options, 'id', newValue)

    },

    /**
     * Emite um evento genérico
     */
    emit (...args) {

      this.$emit.apply(this, ['genericEvent', ...args])

    },

    /**
     * Atualiza dados do Model
     *
     * Não altere o objeto modelo diretamente: ex: this.model = { ... }. Não é permitido pelo Vue (props são imutáveis)
     *
     * @param {Object} values  Valores a serem substituidos
     */
    updateModel (values) {

      // TODO: varrer os schemas e validar se o atributo existe, se não existir ignorar ou levantar exceção de acordo com a configuração

      for (let key in values) {
        if (values.hasOwnProperty(key)) {
          this.$set(this.model, key, values[key])
        }
      }

    },

  },

}