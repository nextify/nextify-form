/**
 * **Created on 13/07/17**
 *
 * src/templates/default/index.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 *     Template Padrão para o Formulário
 *
 */

import smartifyForm from './html/smartifyForm.html'
import fieldsCreator from './html/fieldsCreator.html'
import simple from './engines/simple.html'

import './engines/style.css'

export default {
  smartifyForm,
  fieldsCreator,
  engine: {
    simple,
  },
}
