/**
 * **Created on 13/07/17**
 *
 * src/templates/default/index.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 *   http://getbootstrap.com.br/getting-started/
 *
 *   Template baseado no Bootstrap
 *
 */

import smartifyForm from '../default/html/smartifyForm.html'
import fieldsCreator from '../default/html/fieldsCreator.html'
import simple from './engines/simple.html'

// Jquery
import 'script-loader!./js/jquery-3.2.1.min.js'
import 'script-loader!./js/jquery.blockUI.js'

// Bootstrap
import './bootstrap-3.3.7-dist/css/bootstrap.css'
import './bootstrap-3.3.7-dist/css/bootstrap-theme.css'
import 'script-loader!./bootstrap-3.3.7-dist/js/bootstrap.js'

export default {
  smartifyForm,
  fieldsCreator,
  engine: {
    simple,
  },
  api: {

    /**
     * Eventos Vindo do Form
     *
     * @param form
     * @param event
     * @param eventArgs
     */
    event (form, event, ...eventArgs) {

      if (event === 'loading') {

        $('#smartify-form-simple-overlay').block({
          message: `<h3>${form.options.waitMessage || 'Aguarde...'}</h3>`,
          css: {border: '3px solid #a00'},
        })

      }

      if (event === 'loaded') {
        $('#smartify-form-simple-overlay').unblock()

      }

    },
  },
}
