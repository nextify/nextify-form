# Smartify Form #

[Guia para Desenvolvedor](./docs/development.md)

## Características


* Possibilidade de geração automatica (através de um json descritivo)
* Deve  inserir json ao instanciar
* Pode carregar json do servidor automaticamente via API
* Deve poder  salvar e carregar dados diretamente do servidor
    * Salvar e continuar
* Módular para facil desenvolvimento de novos Templates
* Permite carregar outros formulários via modal (Exemplo, cadastro inline)
    * Importar configuração de outro lugar
* Fields devem ser possível de desenvolver em componentes
* Permitir Máscara (módulo a parte, estudar biblitoeca de terceiros)
* Permitir Validação (Módulo a parte, estudar biblitoeca de terceiros)
* Permitir Filtros  (Módulo a parte, estudar biblitoeca de terceiros)
* Json que descreve formulário deve ser compatível com o Grid
    * Projetar o descritivo previamente
* Analisar criação de novas fases para mascara, validação e Filtros
* Deve ser possível organizar posição dos campos facilmente (Na ultima versão utilizei o grid do bootstrap)
* Deve ter template de facil personalização ( só mudar um parâmetro, muda o layout)
* Campos/ Mascaras/ Validação e Filtros devem ser criado na forma de plugins
    * Ter uma arquivo próprio (exemplo: autocomplete.js na pasta fields) para facil desenvilvimento posterior
    * Mascará é inerente a um field especifico (não ficará separado)
    * Validação, entender como personalizar
* Implementação do layout do schema  estrutural e dados (São 2)
    * Unica forma é mudando o stilo na propriedade, (TESTAR BEM)
* Suporte a WebSocket
    * Edição Compartilhada
* Edição Exlusiva, (Bloquear outras pessoas de editar)
* Preparado para formulario do tipo varias etapas que vai salvando na base (deixar fácil)
* Documentar BEM



# Utilizando Smartify-Form

## Criando Projeto Vue

Aqui vou explicar como criar um novo projeto Vuejs, se você já tem um projeto pronto, pode pular esta etapa

O Vue tem uma ferramenta chamada vue-cli para ser usado para criação de códigos pré definido (scafolding)

**Instale com:**
>	npm install -g vue-cli
	

**Para listar o templates disponíveis usar o comando:**
>	vue list

**Agora** **Você pode criar projeto pré definido para o webpack para testar:**
>	vue init webpack-simple hello-world

**Entra no diretório e executa:**
>	npm install
	

**Com isso vamos ter uma estruturá básica de projeto pré configurado:**
* Webpack 2
* Lslint
* Babel com ES6

**Inicie o projeto para verificar se está tudo certo:**
>	npm run dev


## Dependencias

### Webpack

Necessário instalação e configuração dos seguintes loaders no webpack:

> npm install --save html-loader
> npm install --save vue-loader

Adicionar no webpack.config.js no atributo module.rules:

```javascript
	{
		test: /\.html$/,
		loader: 'html-loader'
	}


```

### Babel

Necessário instalação e configuração do es2015:

> npm install --save babel-preset-es2015

Agora edite o arquivo .babelrc na raiz do projeto:

```
{
  "presets": [
    ["es2015", { "modules": false }]
  ]
}

```


## Instalação e Código Básico

Para instalação basta executar:
> npm install smartify-form

Ou em ambiente de dev:
> npm link smartify-form

Agora importe o módulo e carregue no vuejs como um plugin:

```javascript
import Vue from 'vue'
import App from './App.vue'

import smartifyForm from 'smartify-form'

Vue.use(smartifyForm)

new Vue({
  el: '#app',
  render: h => h(App)
})


```

Agora no template adicione a tag smartify-form:

```html
<smartify-form :options="options"></smartify-form>
```

## Atributos

### Options

* **Tipo:** Objeto
* **Obrigatório:** Sim
* **Descrição:** Configuração do formulário


| Atributo | Descrição                                                                                                                                                                                                                                                                                                                     | Tipo   | Obrigatório | Padrão |
|----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-------------|--------|
| type     | Tipo de formulário que será renderizado, internamente conhecido como engine.  Pode ser: simple, wizard, modal  Permite modularização da implementação do formulário, deixando-o facilmente extensível  Cara tipo de formulário tem seu próprio conjunto de configuração.<br><br>  **Nota:** Apenas simple está implementado no momento | string | não       | simple |
| schemas  | Mesmo que definir schema diretamente no atributo schema (mais na próxima sessão), adicionado por convêniencia, ex: definir url do schema | string \| object | não       | null |
| onSchemasLoaded  | Função que será chamada quando schema tiver sido carregado (Útil para schema vindo do backend, permite fazer alguns ajustes como por exemplo definir propriedades que não podem ser definidas no backend (exemplo: funções ou métodos)| function | não       | undefined |
| apiUrl  | Url da API REST para persistencia, se definido os dados serão salvos e carregados do servidor. Caso contrário apenas eventos serão disparados com os dados do modelo.<br><br> Caso apiUrl não esteja definido, o formulário vai delegar a persistencia dos dados, neste caso será necessário definir o novo id gerado manualmente. (Veja exemplo na sessão Eventos abaixo) | string | não       | null |
| id  | Se definido o formulário entra no modo atualização, recuperando os dados do servidor através deste id | mixed | não  | null |
| waitMessage | Mensagem que será exibida enquanto aguarda uma resposta do servidor | String | Não | 'Aguarde...' |

**Nota:** Alguns atributos do formulário podem ser definidos diretamente no schema. [Refêrencia Schema](./docs/schema.md)

### Options: Engine Simple

| Atributo | Descrição                                                                                                                                                                                                                                                                                                                     | Tipo   | Obrigatório | Padrão |
|----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-------------|--------|
| activeSchema | Esta engine só aceita uma formulário, aqui definimos qual formulário vamos carregar | string | não       | 'default' |
| saveButtonLabel | Label do botão quando é um novo registro | string | não       | 'Salvar' |
| updateButtonLabel | Label do botão quanto está atualizando um registro | string | não       | 'Atualizar' |
| waitMessage | Mensagem que será exibido enquanto aguarda requisição do backend | string | não       | 'Aguarde...' |



### id

O formulário tem dois modos:
* Novo
* Atualização

Quando nenhuma **id** é definido o formulário entra automaticamente no modo novo, portanto se um id for definido entra automaticamente no modo atualização.

Ao salvar um formulário ele entra automaticamente no modo atualização.

Você pode definid o id de um formulário de 3 maneiras, que será carregado na seguinte ordem (se estiver definido na primeira ignora as seguintes)

* 1) Opção id na tag do componente:
```html
<smartify-form id="23"> </smartify-form>
```

* 2) Como atributo de options:

>   options.id = 23

* 3) Definido no modelo no atributo id (pode variar dependendo da Engine)

```html
<smartify-form model="model"> </smartify-form>

model = {
    id: 23
}

```



### Schemas

Schemas (esquema) é o atributo de configuração que definem um ou mais formulários.

Este atributo é compartilhado entre todos os projetos do smartify, seja smartify-form smartify-grid entre outros, simplificando por exemplo a definição de uma tela de Crud.

Todas as propriedades do ou dos formulários serão definidos aqui

É possível definir um ou mais formulários dependendo do tipo escolhido (exemplo: wizard)

Schemas pode ser definido diretamente no cliente ou carregado do backend, neste caso, necessário definir aqui url do backend para carregar schema. Smartify Form entende que se for definido uma String, será considerado URL se for objeto considera schema.

Também pode ser definido em options

[Clique Aqui](https://gitlab.smarti.io/smartify/documentation/blob/master/schema.md) para abrir a referência do schema (Documentação Única)

### model

Em model fica todos os valores gravados no(s) formulário(s)

## Eventos

### loading

Iniciou uma requisição com o Backend

### loaded

Requisição com o backend finalizada


### save

Formulário foi validado e salvo

| Parâmetro | Descrição  | Tipo |
|----------|--------------------|--------|
| form | Instancia do Formulário  | Object |
| model | Cópia do modelo recem salvo com todos os dados  | Object |
| isNew | Se é um novo registro ou uma atualização  | boolean |


Exemplo de como definir um novo id, quando estiver com persistencia desativada:

```html
<smartify-form :options="options" :model="model" @save="onSave"></smartify-form>
```

```javascript

 export default {
    name: 'app',
    methods: {

      onSave (form, model, isNew) {

        console.log(model)
        if (form.isNew) {

          // Utilize aqui seu gerador de id e salve no registro
          form.id = 23
        }

      },

    },
 }

```


### saveSuccess

Formulário foi validado, salvo e persistido no backend

| Parâmetro | Descrição  | Tipo |
|----------|--------------------|--------|
| form | Instancia do Formulário  | Object |
| id | Valor do Identificador do novo registro  | Mixed |
| model | Cópia do modelo recem salvo com todos os dados  | Object |
| isNew | Se é um novo registro ou uma atualização  | boolean |

### validated

Se ocorreu um evento de validação:

| Parâmetro | Descrição  | Tipo |
|----------|--------------------|--------|
| form | Instancia do Formulário  | Object |
| isValid | Se foi validado | Boolean |
| errors | Lista de Erros, com informação do campo inválido e descrição do erro | Array |

**IMPORTANTE:** Cada Engine pode ter seu conjunto de eventos distinto

TODO: Documentar aqui, eventos de cada engine


## Customizando Template



* TODO: Explicar técnica de utilizar script css não compatível com webpack

* TODO: Explicar o conceito de api